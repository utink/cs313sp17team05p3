package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;

import java.util.List;

import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;
	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final Stroke c) {
		int color = paint.getColor();
		Style style = paint.getStyle();
		paint.setColor(c.getColor());
		paint.setStyle(Style.STROKE);
		c.getShape().accept(this);
		paint.setColor(color);
		paint.setStyle(style);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		Style style = paint.getStyle();
		paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		paint.setStyle(style);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		List <? extends Shape> shapes = g.getShapes();
		for (int i = 0; i < shapes.size(); i++){
			shapes.get(i).accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(),l.getY());
		l.getShape().accept(this);
		canvas.translate(-l.getX(),-l.getY());
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0,0,r.getWidth(),r.getHeight(),paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		Style style = paint.getStyle();
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(style);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		List<? extends Point> points = s.getPoints();
		final float[] pts = new float[(points.size()*4)];
		int i = 0;
		int j = 0;
		while (j < pts.length){
			pts[j] = points.get(i).getX();
			j++;
			pts[j] = points.get(i).getY();
			j++;
			i++;
			if (j % 4 ==0)
			{
				i=i-1;
			}
			if (j==pts.length-2)
			{
				i=0;
				pts[j]=points.get(i).getX();
				j++;
				pts[j]=points.get(i).getY();
				j++;
			}
		}
		canvas.drawLines(pts, paint);
		return null;
	}
}
