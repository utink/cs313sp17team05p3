package edu.luc.etl.cs313.android.shapes.model;

import java.util.List;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {


	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {
		List<? extends Shape> shapes = g.getShapes();
		Location temp = shapes.get(0).accept(this);
		int x,y,xmin,ymin,xmax=0,ymax = 0;
		xmin = temp.getX();
		ymin = temp.getY();

        for (int i =1; i < shapes.size(); i++){
			temp = shapes.get(i).accept(this);
			Rectangle dim = (Rectangle) temp.getShape();
            x = temp.getX();
			y = temp.getY();

            if (x + dim.getWidth() > xmax){ xmax = x + dim.getWidth(); }
			if (y + dim.getHeight() > ymax){ ymax = y + dim.getHeight(); }
			if (x < xmin){ xmin = x; }
			if (y < ymin){ ymin = y; }
		}
		return new Location(xmin, ymin, new Rectangle(xmax-xmin, ymax-ymin));
	}

	@Override
	public Location onLocation(final Location l) {
		Location box = l.getShape().accept(this);
		Shape bound = box.getShape();
		int x = l.getX() + box.getX();
		int y = l.getY() + box.getY();
		return new Location(x, y, bound);
	}
	//not completely confident this holds for polygons, (but it has to because polygons are collections of points?)
	//but given our definition of circle and rectangle it holds there I think

	@Override
	public Location onRectangle(final Rectangle r) {
		final int width = r.getWidth();
		final int height = r.getHeight();
		return new Location(0, 0, new Rectangle(width,height));
	}

	@Override
	public Location onStroke(final Stroke c) {
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		List<? extends Point> points = s.getPoints();
		//Point temp;
		int x = points.get(0).getX();
		int y = points.get(0).getY();
		int xmax = x;
		int xmin = x;
		int ymax = y;
		int ymin = y;

		for (int i = 0; i < points.size(); i++){
			Point temp = points.get(i);
			x = temp.getX();
			y = temp.getY();
			if (x > xmax){ xmax = x; }
			if (y > ymax){ ymax = y; }
			if (x < xmin){ xmin = x; }
			if (y < ymin){ ymin = y; }
		}
		return new Location(xmin, ymin, new Rectangle(xmax-xmin,ymax-ymin));
	}
}
